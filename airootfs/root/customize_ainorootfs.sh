#!/bin/zsh

cd /home/noroot/; wget https://aur.archlinux.org/cgit/aur.git/snapshot/package-query.tar.gz
cd /home/noroot/; wget https://aur.archlinux.org/cgit/aur.git/snapshot/yaourt.tar.gz

tar xvf /home/noroot/package-query.tar.gz
tar xvf /home/noroot/yaourt.tar.gz
rm /home/noroot/*.tar.gz

cd /home/noroot/package-query/; makepkg; sudo pacman -U *.pkg.tar.xz --noconfirm
rm -r /home/noroot/package-query/

cd /home/noroot/yaourt; makepkg; sudo pacman -U *.pkg.tar.xz --noconfirm
rm -r /home/noroot/yaourt/

yaourt -S i3-gaps --noconfirm
yaourt -S google-chrome --noconfirm
yaourt -S ttf-dejavu-sans-mono-powerline-git --noconfirm
yaourt -S ttf-roboto --noconfirm
yaourt -S ttf-material-design-icons-git --noconfirm
