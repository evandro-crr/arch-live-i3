#!/bin/bash

set -e -u

sed -i 's/#\(en_US\.UTF-8\)/\1/' /etc/locale.gen
sed -i 's/#\(pt_BR\.*\)/\1/' /etc/locale.gen
locale-gen

ln -sf /usr/share/zoneinfo/America/Sao_Paulo /etc/localtime

usermod -s /usr/bin/zsh root
cp -aT /etc/skel/ /root/
chmod 700 /root

sed -i "1s/^/Server = http:\/\/pet\.inf\.ufsc\.br\/mirrors\/archlinux\/\$repo\/os\/\$arch\n/" /etc/pacman.d/mirrorlist
sed -i 's/#\(PermitRootLogin \).\+/\1yes/' /etc/ssh/sshd_config
sed -i "s/#Server/Server/g" /etc/pacman.d/mirrorlist
sed -i 's/#\(Storage=\)auto/\1volatile/' /etc/systemd/journald.conf

systemctl enable pacman-init.service choose-mirror.service
systemctl set-default graphical.target

sed -i "s/#\(auto_login \).\+/\1yes/" /etc/slim.conf
sed -i "s/#\(default_user \).\+/\1root/" /etc/slim.conf

systemctl enable slim
systemctl enable dhcpcd
systemctl enable NetworkManager

vim +PluginInstall +qall
mv /root/.vimrc. /root/.vimrc
~/.vim/bundle/YouCompleteMe/install.sh

pacman-key --init
pacman-key --populate archlinux

useradd -m -s /usr/bin/zsh noroot

mv /root/customize_ainorootfs.sh /home/noroot/

chown -R noroot:noroot /home/noroot/

sed -i "s/# \(ALL ALL=(ALL) \).\+/\1NOPASSWD: ALL/" /etc/sudoers

su -c "/home/noroot/customize_ainorootfs.sh" -l noroot
rm /home/noroot/customize_ainorootfs.sh
